FROM python:3.11-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -yqq jq \
    && pip3 install --no-cache-dir --upgrade \
        awscli aws-sam-cli pycodestyle yamllint virtualenv \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
